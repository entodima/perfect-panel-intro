<?php

namespace App\Http\Controllers\API\V1;

use App\Factories\CurrenciesCollectionFactory;
use App\Formatters\CurrencyFormatter;
use App\Http\Controllers\Controller;
use App\Models\DTO\CurrencyDTO;
use App\Services\AddCommissionToCurrency;
use Illuminate\Http\JsonResponse;
use JsonException;

class RatesController extends Controller
{
    private string $blockchainUrl = 'https://www.blockchain.com/ticker';
    private CurrenciesCollectionFactory $currenciesCollectionFactory;
    private AddCommissionToCurrency $addCommission;
    private CurrencyFormatter $currencyFormatter;

    public function __construct(CurrenciesCollectionFactory $currenciesCollectionFactory, AddCommissionToCurrency $addCommission, CurrencyFormatter $currencyFormatter)
    {
        $this->currenciesCollectionFactory = $currenciesCollectionFactory;
        $this->addCommission = $addCommission;
        $this->currencyFormatter = $currencyFormatter;
    }

    /**
     * @throws JsonException
     */
    public function getRates(?string $tickers): JsonResponse
    {
        $jsonCurrencies = json_decode(file_get_contents($this->blockchainUrl), true, 3, JSON_THROW_ON_ERROR);
        $currencies = $this->currenciesCollectionFactory->fromArJson($jsonCurrencies);

        if (isset($tickers)) { // если заданы валюты, то фильтруем по ним
            $arTickers = explode(',', $tickers);
            $currencies = $currencies->filter(static function(CurrencyDTO $currency) use ($arTickers) {
                return in_array($currency->getTicker(), $arTickers);
            });
        }

        $this->addCommission->addToCollectionOfCurrencies($currencies);

        $currencies = $currencies->sortBy(function (CurrencyDTO $currency) {
            return $currency->getPrice();
        });

        return response()->json(
            [
                'status' => 'success',
                'code' => 200,
                'data' => $this->currencyFormatter->forRatesResponse($currencies)
            ]
        );
    }
}
