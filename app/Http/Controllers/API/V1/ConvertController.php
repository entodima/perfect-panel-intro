<?php

namespace App\Http\Controllers\API\V1;

use App\Factories\CurrenciesCollectionFactory;
use App\Formatters\ConvertedCurrencyFormatter;
use App\Http\Controllers\Controller;
use App\Models\DTO\CurrencyDTO;
use App\Services\AddCommissionToCurrency;
use App\Services\ConvertCurrency;
use Exception;
use Illuminate\Http\JsonResponse;

class ConvertController extends Controller
{
    private string $blockchainUrl = 'https://www.blockchain.com/ticker';
    private CurrenciesCollectionFactory $currenciesCollectionFactory;
    private AddCommissionToCurrency $addCommission;
    private ConvertCurrency $convertCurrency;
    private ConvertedCurrencyFormatter $convertedCurrencyFormatter;

    public function __construct(
        CurrenciesCollectionFactory $currenciesCollectionFactory,
        AddCommissionToCurrency $addCommission,
        ConvertCurrency $convertCurrency,
        ConvertedCurrencyFormatter $convertedCurrencyFormatter
    ) {
        $this->currenciesCollectionFactory = $currenciesCollectionFactory;
        $this->addCommission = $addCommission;
        $this->convertCurrency = $convertCurrency;
        $this->convertedCurrencyFormatter = $convertedCurrencyFormatter;
    }

    /**
     * @throws Exception
     */
    public function convert(string $tickerFrom, string $tickerTo, float $value): JsonResponse
    {
        if ($tickerFrom === $tickerTo || $value <= 0.0) {
            throw new Exception();
        }
        $value = round($value, 2);

        $jsonCurrencies = json_decode(file_get_contents($this->blockchainUrl), true, 3, JSON_THROW_ON_ERROR);
        $currencies = $this->currenciesCollectionFactory->fromArJson($jsonCurrencies);

        if ($tickerFrom === 'BTC') { // переводим в валюту
            $currency = $currencies->filter(static function(CurrencyDTO $currency) use ($tickerTo) {
                return $currency->getTicker() === $tickerTo;
            })->First();
            $convertedCurrency = $this->convertCurrency->fromBTC($tickerFrom, $tickerTo, $value, $currency);
        } else { // переводим в биткоин
            $currency = $currencies->filter(static function(CurrencyDTO $currency) use ($tickerFrom) {
                return $currency->getTicker() === $tickerFrom;
            })->First();
            $convertedCurrency = $this->convertCurrency->toBTC($tickerFrom, $tickerTo, $value, $currency);
        }

        $convertedCurrency = $this->addCommission->addToConvertedCurrency($convertedCurrency);

        return response()->json(
            [
                'status' => 'success',
                'code'   => 200,
                'data'   => $this->convertedCurrencyFormatter->forConvertResponse($convertedCurrency)
            ]
        );
    }
}
