<?php

namespace App\Services;

use App\Models\DTO\ConvertedCurrencyDTO;
use App\Models\DTO\CurrencyDTO;
use Illuminate\Support\Collection;

class AddCommissionToCurrency
{
    private static float $commissionScaleFactor = .02;

    public function addToCollectionOfCurrencies(Collection $currencies): Collection
    {
        foreach ($currencies as $currency) {
            /** @var CurrencyDTO $currency */
            $currency->setCurrency($currency->getPrice() * (1 + self::$commissionScaleFactor));
        }

        return $currencies;
    }

    public function addToConvertedCurrency(ConvertedCurrencyDTO $convertedCurrency): ConvertedCurrencyDTO
    {
        $convertedCurrency->setConvertedValue($convertedCurrency->getConvertedValue() * (1 - self::$commissionScaleFactor));
        $convertedCurrency->setRate($convertedCurrency->getRate() * (1 - self::$commissionScaleFactor));
        return $convertedCurrency;
    }
}
