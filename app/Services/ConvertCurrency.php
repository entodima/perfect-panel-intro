<?php

namespace App\Services;

use App\Models\DTO\ConvertedCurrencyDTO;
use App\Models\DTO\CurrencyDTO;

class ConvertCurrency
{
    public function fromBTC(string $tickerFrom, string $tickerTo, float $value, CurrencyDTO $currencyWithPrice): ConvertedCurrencyDTO
    {
        return new ConvertedCurrencyDTO(
            $tickerFrom,
            $tickerTo,
            $value,
            $currencyWithPrice->getPrice() * $value,
            $currencyWithPrice->getPrice()
        );
    }

    public function toBTC(string $tickerFrom, string $tickerTo, float $value, CurrencyDTO $currencyWithPrice): ConvertedCurrencyDTO
    {
        return new ConvertedCurrencyDTO(
            $tickerFrom,
            $tickerTo,
            $value,
            1 / $currencyWithPrice->getPrice() * $value,
            1 / $currencyWithPrice->getPrice()
        );
    }
}
