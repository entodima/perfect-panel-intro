<?php

namespace App\Formatters;

use App\Models\DTO\CurrencyDTO;
use Illuminate\Support\Collection;

class CurrencyFormatter
{
    public function forRatesResponse(Collection $currencies): array
    {
        $arCurrencies = [];

        foreach ($currencies as $currency) {
            /** @var CurrencyDTO $currency */
            $price = round($currency->getPrice(), 2);
            $arCurrencies[$currency->getTicker()] = $price;
        }

        return $arCurrencies;
    }
}
