<?php

namespace App\Formatters;

use App\Models\DTO\ConvertedCurrencyDTO;

class ConvertedCurrencyFormatter
{
    public function forConvertResponse(ConvertedCurrencyDTO $convertedCurrency): array
    {
        if ($convertedCurrency->getFrom() === 'BTC') {
            $precision = 2;
        } else {
            $precision = 10;
        }
        return [
            'currency_from'   => $convertedCurrency->getFrom(),
            'currency_to'     => $convertedCurrency->getTo(),
            'value'           => $convertedCurrency->getValue(),
            'converted_value' => round($convertedCurrency->getConvertedValue(), $precision),
            'rate'            => $convertedCurrency->getRate()
        ];
    }
}
