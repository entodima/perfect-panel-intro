<?php

namespace App\Factories;

use App\Exceptions\FieldNotSetException;
use App\Models\DTO\CurrencyDTO;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class CurrenciesCollectionFactory
{
    public function fromArJson(array $arJsons): Collection
    {
        $currencies = new Collection();
        foreach ($arJsons as $arJson) {
            try {
                if (!isset($arJson['symbol'])) {
                    throw new FieldNotSetException('Не задано свойство "symbol"');
                }
                if (!isset($arJson['last'])) {
                    throw new FieldNotSetException('Не задано свойство "last"');
                }
                $currencies->add(
                    new CurrencyDTO(
                        $arJson['symbol'],
                        $arJson['last']
                    )
                );
            } catch (FieldNotSetException $exception) {
                Log::error($exception->getMessage(), ['trace' => $exception->getTrace()]);
            }
        }
        return $currencies;
    }
}
