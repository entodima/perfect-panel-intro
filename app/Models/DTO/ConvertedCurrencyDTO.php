<?php

namespace App\Models\DTO;

class ConvertedCurrencyDTO
{
    private string $currencyFrom;
    private string $currencyTo;
    private float $value;
    private float $convertedValue;
    private float $rate;

    public function __construct(string $currencyFrom, string $currencyTo, float $value, float $convertedValue, float $rate)
    {
        $this->currencyFrom = $currencyFrom;
        $this->currencyTo = $currencyTo;
        $this->value = $value;
        $this->convertedValue = $convertedValue;
        $this->rate = $rate;
    }

    public function getConvertedValue(): float
    {
        return $this->convertedValue;
    }

    public function setConvertedValue(float $convertedValue): void
    {
        $this->convertedValue = $convertedValue;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function setRate(float $rate): void
    {
        $this->rate = $rate;
    }

    public function getFrom(): string
    {
        return $this->currencyFrom;
    }

    public function getTo(): string
    {
        return $this->currencyTo;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
