<?php

namespace App\Models\DTO;

class CurrencyDTO
{
    private string $ticker;
    private float $price;

    public function __construct(string $ticker, float $price)
    {
        $this->ticker = $ticker;
        $this->price = $price;
    }

    public function getTicker(): string
    {
        return $this->ticker;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setCurrency(float $price): void
    {
        $this->price = $price;
    }
}
