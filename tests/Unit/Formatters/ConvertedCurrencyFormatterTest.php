<?php

namespace Tests\Unit\Formatters;

use App\Formatters\ConvertedCurrencyFormatter;
use App\Models\DTO\ConvertedCurrencyDTO;
use PHPUnit\Framework\TestCase;

class ConvertedCurrencyFormatterTest extends TestCase
{
    private ConvertedCurrencyFormatter $currencyFormatter;

    protected function setUp(): void
    {
        $this->currencyFormatter = new ConvertedCurrencyFormatter();
    }

    /**
     * @covers \App\Formatters\ConvertedCurrencyFormatter::forConvertResponse
     * @dataProvider forConvertResponseProvider
     *
     * @param ConvertedCurrencyDTO $currency
     * @param array $expected
     */
    public function testForConvertResponse(ConvertedCurrencyDTO $currency, array $expected): void
    {
        $this->assertEquals($expected, $this->currencyFormatter->forConvertResponse($currency));
    }

    public function forConvertResponseProvider(): array
    {
        return [
            'btc-usd' => [
                '$currency' => new ConvertedCurrencyDTO(
                    'BTC',
                    'USD',
                    1,
                    59813.6532,
                    59813.6532
                ),
                '$expected' => [
                    'currency_from' => 'BTC',
                    'currency_to' => 'USD',
                    'value' => 1.0,
                    'converted_value' => 59813.65,
                    'rate' => 59813.6532
                ]
            ],
            'usd-btc' => [
                '$currency' => new ConvertedCurrencyDTO(
                    'USD',
                    'BTC',
                    100.0,
                    0.001605653473,
                    0.00001605653473
                ),
                '$expected' => [
                    'currency_from' => 'USD',
                    'currency_to' => 'BTC',
                    'value' => 100.0,
                    'converted_value' => 0.0016056535,
                    'rate' => 0.00001605653473
                ]
            ]
        ];
    }
}
