<?php

namespace Tests\Unit\Formatters;

use App\Formatters\CurrencyFormatter;
use App\Models\DTO\CurrencyDTO;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class CurrencyFormatterTest extends TestCase
{
    private CurrencyFormatter $formatter;

    protected function setUp(): void
    {
        $this->formatter = new CurrencyFormatter();
    }

    /**
     * @covers CurrencyFormatter::forRatesResponse
     * @dataProvider forRatesProvider
     *
     * @param $data
     * @param $expected
     */
    public function testForRatesResponse($data, $expected): void
    {
        $this->assertEquals($expected, $this->formatter->forRatesResponse($data));
    }

    public function forRatesProvider(): array
    {
        return [
            'correct' => [
                'data' => new Collection(
                    [
                        new CurrencyDTO('BRL', 339837.964),
                        new CurrencyDTO('AUD', 82147.97)
                    ]
                ),
                'expected' => [
                    'BRL' => 339837.96,
                    'AUD' => 82147.97
                ]
            ]
        ];
    }
}
