<?php

namespace Tests\Unit\Services;

use App\Models\DTO\ConvertedCurrencyDTO;
use App\Models\DTO\CurrencyDTO;
use App\Services\ConvertCurrency;
use PHPUnit\Framework\TestCase;

class ConvertCurrencyTest extends TestCase
{
    private ConvertCurrency $convert;

    protected function setUp(): void
    {
        $this->convert = new ConvertCurrency();
    }

    /**
     * @covers       ConvertCurrency::fromBTC
     * @dataProvider fromBTCProvider
     *
     * @param string $tickerFrom
     * @param string $tickerTo
     * @param float $value
     * @param CurrencyDTO $currency
     * @param ConvertedCurrencyDTO $expected
     */
    public function testFromBTC(string $tickerFrom, string $tickerTo, float $value, CurrencyDTO $currency, ConvertedCurrencyDTO $expected): void
    {
        $this->assertEquals($expected, $this->convert->fromBTC($tickerFrom, $tickerTo, $value, $currency));
    }

    public function fromBTCProvider(): array
    {
        return [
            [
                '$tickerFrom' => 'BTC',
                '$tickerTo' => 'USD',
                '$value' => 1,
                '$currency' => new CurrencyDTO('USD', 61034.34),
                '$expected' => new ConvertedCurrencyDTO(
                    'BTC',
                    'USD',
                    1,
                    61034.34,
                    61034.34
                )
            ]
        ];
    }

    /**
     * @covers ConvertCurrency::toBTC
     * @dataProvider toBTCProvider
     *
     * @param string $tickerFrom
     * @param string $tickerTo
     * @param float $value
     * @param CurrencyDTO $currency
     * @param ConvertedCurrencyDTO $expected
     */
    public function testToBTC(string $tickerFrom, string $tickerTo, float $value, CurrencyDTO $currency, ConvertedCurrencyDTO $expected): void
    {
        $this->assertEquals($expected, $this->convert->toBTC($tickerFrom, $tickerTo, $value, $currency));
    }

    public function toBTCProvider(): array
    {
        return [
            [
                '$tickerFrom' => 'USD',
                '$tickerTo' => 'BTC',
                '$value' => 100,
                '$currency' => new CurrencyDTO('USD', 61034.34),
                '$expected' => new ConvertedCurrencyDTO(
                    'USD',
                    'BTC',
                    100,
                    0.001638421911,
                    0.00001638421911
                )
            ]
        ];
    }
}
