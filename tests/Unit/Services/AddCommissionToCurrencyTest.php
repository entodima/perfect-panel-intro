<?php

namespace Tests\Unit\Services;

use App\Models\DTO\ConvertedCurrencyDTO;
use App\Models\DTO\CurrencyDTO;
use App\Services\AddCommissionToCurrency;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class AddCommissionToCurrencyTest extends TestCase
{

    private AddCommissionToCurrency $addCommision;

    protected function setUp(): void
    {
        $this->addCommision = new AddCommissionToCurrency();
    }

    /**
     * @covers \App\Services\AddCommissionToCurrency::addToCollectionOfCurrencies
     * @dataProvider addToCollectionProvider
     */
    public function testAddToCollectionOfCurrencies(Collection $data, Collection $expected): void
    {
        $this->assertEquals($expected, $this->addCommision->addToCollectionOfCurrencies($data));
    }

    public function addToCollectionProvider(): array
    {
        return [
            '' => [
                'data' => new Collection(
                    [
                        new CurrencyDTO('BRL', 339837.96),
                        new CurrencyDTO('AUD', 82147.97)
                    ]
                ),
                'expected' => new Collection(
                    [
                        new CurrencyDTO('BRL', 346634.7192),
                        new CurrencyDTO('AUD', 83790.9294)
                    ]
                )
            ],
            'empty' => [
                'data' => new Collection(),
                'expected' => new Collection()
            ]
        ];
    }

    /**
     * @covers AddCommissionToCurrency::addToConvertedCurrency()
     * @dataProvider addToConvertCurrencyProvider
     *
     * @param ConvertedCurrencyDTO $before
     * @param ConvertedCurrencyDTO $after
     */
    public function testAddToConvertedCurrency(ConvertedCurrencyDTO $before, ConvertedCurrencyDTO $after): void
    {
        $this->assertEquals($after, $this->addCommision->addToConvertedCurrency($before));
    }

    public function addToConvertCurrencyProvider(): array
    {
        return [
            'btc-usd' => [
                'before' => new ConvertedCurrencyDTO(
                    'BTC',
                    'USD',
                    1,
                    61034.34,
                    61034.34
                ),
                'after' => new ConvertedCurrencyDTO(
                    'BTC',
                    'USD',
                    1,
                    59813.6532,
                    59813.6532
                )
            ],
            'usd-btc' => [
                'before' => new ConvertedCurrencyDTO(
                    'USD',
                    'BTC',
                    100,
                    0.001638421911,
                    0.00001638421911
                ),
                'after' => new ConvertedCurrencyDTO(
                    'USD',
                    'BTC',
                    100,
                    0.001605653473,
                    0.00001605653473
                )
            ]
        ];
    }
}
