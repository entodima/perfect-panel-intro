<?php

namespace Tests\Unit\Factories;

use App\Models\DTO\CurrencyDTO;
use App\Factories\CurrenciesCollectionFactory;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class CurrenciesCollectionFactoryTest extends TestCase
{
    private CurrenciesCollectionFactory $curreciesCollectionFactory;

    protected function setUp(): void
    {
        $this->curreciesCollectionFactory = new CurrenciesCollectionFactory();
    }

    /**
     * @covers       CurrenciesCollectionFactory::fromArJson
     * @dataProvider fromJsonProvider
     *
     * @param $data
     * @param $expected
     */
    public function testFromJson($data, $expected): void
    {
        $this->assertEquals($expected, $this->curreciesCollectionFactory->fromArJson($data));
    }

    /**
     * @return array[]
     */
    public function fromJsonProvider(): array
    {
        return [
            'correct-data' => [
                'data' => [
                    'AUD' => [
                        '15m'    => 82147.97,
                        'last'   => 82147.97,
                        'buy'    => 82147.97,
                        'sell'   => 82147.97,
                        'symbol' => 'AUD',
                    ],
                    'BRL' => [
                        '15m'    => 339837.96,
                        'last'   => 339837.96,
                        'buy'    => 339837.96,
                        'sell'   => 339837.96,
                        'symbol' => 'BRL',
                    ]
                ],
                'expected' => new Collection([
                    new CurrencyDTO('AUD', 82147.97),
                    new CurrencyDTO('BRL', 339837.96)
                ])
            ],
            'empty' => [
                'data' => [],
                'expected' => new Collection()
            ]
        ];
    }
}
