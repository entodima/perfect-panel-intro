<?php

use App\Http\Controllers\API\V1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['custom_auth']], function() {
    Route::get('/v1', function (Request $request) {
        try {
            if ($request->get('method') === 'rates') {
                return app()->make(V1\RatesController::class)->callAction('getRates', [
                    $request->get('currency')
                ]);
            }
        } catch (Throwable) {
            return response()->json(
                [
                    'status'  => 'error',
                    'code'    => 403,
                    'message' => 'Invalid token'
                ],
                403
            );
        }
    });

    Route::post('/v1', function (Request $request) {
        try {
            if ($request->get('method') === 'convert') {
                return app()->make(V1\ConvertController::class)->callAction('convert', [
                    $request->get('currency_from'),
                    $request->get('currency_to'),
                    $request->get('value')
                ]);
            }
        } catch (Throwable) {
            return response()->json(
                [
                    'status'  => 'error',
                    'code'    => 403,
                    'message' => 'Invalid token'
                ],
                403
            );
        }
    });
});





