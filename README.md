## Задание 1

```mysql
SELECT
    users.id AS "ID",
    CONCAT(users.first_name, ' ', users.last_name) AS "Name",
    bks.author AS "Author",
    (
        SELECT
            GROUP_CONCAT(books.name)
        FROM
            books
        WHERE
            books.author = bks.author) AS "Books"
FROM
    users
        INNER JOIN user_books ON users.id = user_books.user_id
        INNER JOIN books bks ON bks.id = user_books.book_id
WHERE
    users.age < 17
  AND users.age > 7
GROUP BY
    users.id,
    bks.author
HAVING
        users.id IN(
        SELECT
            user_id FROM user_books
        GROUP BY
            user_id
        HAVING
            COUNT(user_id) = 2)
```

## Задание 2

Для запуска использовал docker-compose с настройками из https://github.com/aschmelyun/docker-compose-laravel.

Для авторизации используется middleware: App\Http\Middleware\ApiAuthentication

Роутинг для api находится в /routes/api.php. 
В нем автоматически происходит авторизация из middleware, получение свойств из url и подключаются необходимые обработчики.

Обработчики находятся в app/Http/Controllers/API/V1.<br>
RatesController отвечает за обработку вывода всех курсов с комиссией. <br>
ConvertController отвечает за обработку конвертации валюты с учетом комиссии.

Для всех фабрик, форматеров и сервисов написаны тесты. Располагаются в папке tests/Unit.

### Для запуска необходимо:
- развернуть сервер
- провести миграцию `php artisan migrate`
- добавить пользователя в таблицу users, столбец api_token будет использоваться далее для авторизации
- выполнить запрос

### Скриншоты
**Все курсы с учетом комиссии - успешная авторизация**
![](https://gitlab.com/entodima/perfect-panel-intro/-/raw/master/img/rates-auth.png)

**Все курсы с учетом комиссии - ошибка авторизации**
![](https://gitlab.com/entodima/perfect-panel-intro/-/raw/master/img/rates-error.png)

**Обмен валюты c учетом комиссии - успешная авторизация**
![](https://gitlab.com/entodima/perfect-panel-intro/-/raw/master/img/convert-auth.png)

**Обмен валюты c учетом комиссии - ошибка авторизации**
![](https://gitlab.com/entodima/perfect-panel-intro/-/raw/master/img/convert-error.png)

**Запись в таблице "users"**
![](https://gitlab.com/entodima/perfect-panel-intro/-/raw/master/img/db.png)

**Тесты**
![](https://gitlab.com/entodima/perfect-panel-intro/-/raw/master/img/tests.png)
